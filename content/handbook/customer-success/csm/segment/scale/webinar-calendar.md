---

title: "CSM/CSE Webinar & Hands-On Labs Calendar"
---
# On this page



View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---
# Upcoming Events

We’d like to invite you to our free upcoming webinars and labs in the months of February and March 2024.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## February 2024

### AMER Time Zone Webinars & Labs

#### GitLab Runner Fundamentals and What You Need to Know
##### February 9th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Runners are a requirement for operating GitLab CI/CD. Learn about Runner architecture, options for deployments, supported operating systems, and optimizing them for most efficient usage. We will be discussing best practices, decisions that you need to make before deploying at scale, and options you have for monitoring fleets of runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_uc6Q1ZY2SpmQxRAiQUc-jA#/registration)

#### Advanced CI/CD
##### February 13th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_uJms-QWpRXuNF8J1DXLMjw#/registration)

#### Hands-On Advanced GitLab CI Lab 
##### February 14th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

Join us for a hands-on lab where we will go into advanced GitLab CI capabilities that allow customers to simplify pipeline yml code and optimize the execution time of pipelines.

We will cover:
- Storing build resources in GitLab registries
- Managing artifacts, dependencies, and environment variables between jobs
- Running a job multiple times in parallel with different variables
- Triggering downstream pipelines

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_h-Q7kP6CRNmbPYnE4h-DVg)

#### Security and Compliance
##### February 21st, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_kNAaDYm_TgiUZTXV7HtfKA#/registration)

### Jira to GitLab: Helping you transition to planning with GitLab
##### February 27th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab offers robust team planning capabilities that work directly with the DevOps lifecycle. Keeping teams in a single application offers operational and financial advantages. Many customers inquire about what a transition would look like from Jira Software to GitLab to team planning. This webinar will start to outline the differences in functionality and advantages gained by using GitLab. 

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_rbTleneZRSi4QkOPOV-Rwg#/registration)

#### AI in DevSecOps - GitLab Hands-On Lab
##### February 28th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

Join us for a hands-on GitLab AI lab where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_tdfeUNKoRwqid6SuRfNfWw)

### EMEA Time Zone Webinars & Labs

#### GitLab Runner Fundamentals and What You Need to Know
##### February 9th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

Runners are a requirement for operating GitLab CI/CD. Learn about Runner architecture, options for deployments, supported operating systems, and optimizing them for most efficient usage. We will be discussing best practices, decisions that you need to make before deploying at scale, and options you have for monitoring fleets of runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_U_lPFBRzQSmgYOYcgq7dpA#/registration)

#### Advanced CI/CD
##### February 13th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_mo2QLf2pToqaT3U7n6XqKw#/registration)

#### Hands-On Advanced GitLab CI Lab 
##### February 14th, 2023 at 10:00AM-12:00PM UTC / 11:00AM-1:00PM CET

Join us for a hands-on lab where we will go into advanced GitLab CI capabilities that allow customers to simplify pipeline yml code and optimize the execution time of pipelines.

We will cover:
- Storing build resources in GitLab registries
- Managing artifacts, dependencies, and environment variables between jobs
- Running a job multiple times in parallel with different variables
- Triggering downstream pipelines

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_HCHR8oBPTAGkeI8IP_bMgQ)

#### Security and Compliance
##### February 21st, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_3gZXFUp6RmeHB_Nu6ubjMw#/registration)

### Jira to GitLab: Helping you transition to planning with GitLab
##### February 27th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

GitLab offers robust team planning capabilities that work directly with the DevOps lifecycle. Keeping teams in a single application offers operational and financial advantages. Many customers inquire about what a transition would look like from Jira Software to GitLab to team planning. This webinar will start to outline the differences in functionality and advantages gained by using GitLab. 

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_C1ILuQPKQ4GDdZ0T18pnMQ#/registration)

#### AI in DevSecOps - GitLab Hands-On Lab
##### February 28th, 2023 at 10:00AM-12:00PM UTC / 11:00AM-1:00PM CET

Join us for a hands-on GitLab AI lab where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_cGKf3oS2RmS384OPjwy03Q)

### APJ Time Zone Webinars & Labs

#### Intro to GitLab
##### February 27th, 2024 at 11:30AM-12:30PM India / 1:00-2:00PM Indonesia / 2:00-3:00PM Singapore / 5:00-6:00PM Sydney

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_0NaJQR2VT7SZS6pgfOyT5w#/registration)

#### Intro to CI/CD
##### February 29th, 2024 at 11:30AM-12:30PM India / 1:00-2:00PM Indonesia / 2:00-3:00PM Singapore / 5:00-6:00PM Sydney

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_79I8V65BRk6msdFlzcDFRQ#/registration)

## March 2024

### APJ Time Zone Webinars & Labs

#### Advanced CI/CD
##### March 5th, 2024 at 11:30AM-12:30PM India / 1:00-2:00PM Indonesia / 2:00-3:00PM Singapore / 5:00-6:00PM Sydney

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_O075aLVnRiOwhJA3rC0EyA#/registration)

#### Security and Compliance
##### March 6th, 2024 at 7:30-8:30AM India / 9:00-10:00AM Indonesia / 10:00-11:00AM Singapore / 1:00-2:00PM Sydney

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_QDruIUyuSZOMx59qkHX9rA#/registration)


Check back later for more webinars & labs! 


