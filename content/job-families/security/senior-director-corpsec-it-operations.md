---
title: Senior Director, Corporate Security & IT Operations
---

The Director, IT Operations Job Family leads a team of a highly-collaborative and results-oriented IT team members tasked with delivering global IT services across the company. The Director, IT Operations is in charge of scaling, increasing performance, and providing great team member experience in order to help drive forward business success based on world class infrastructure & operations.

## Levels

### Senior Director, Corporate Security & IT Operations

The Director, Corporate Security & IT Operations reports to the [CISO](https://handbook.gitlab.com/job-families/chief-information-security-officer/)

#### Senior Director, Corporate Security & IT Operations Job Grade

The Senior Director, Corporate Security & IT Operations is a [level 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Director, Corporate Security & IT Operations Responsibilities

- Head a multi-continent team in an all-remote Organization
- Build and lead a team of Corporate Security & IT Operations and Service Management staff
- Create the Strategy and roadmap, effectively defining and delivering on new SOPs, process improvements, and projects
- ITIL: Lead the design, implementation, and enhancement of ITIL processes like Incident, Problem, Change, Configuration, and Release Management to achieve operational excellence
- Service Desk: Foster customer-centricity in IT operations, manage a responsive IT service desk, focusing on end-user satisfaction. Prioritize incident resolution, escalate effectively, and strategize for optimal first-call resolution
- Identity & Access: Oversee secure identity and access management and data protection programs in conjunction with the InfoSec team to enforce policies and uphold data integrity.
- Manage the IT Ops departmental budget
- Vendor Management: Build / maintain relationships with software and hardware vendors and service providers
- Directly participate in IT Security improvements and support as required (i.e., be hands-on)
- Establish seamless processes to onboard, change and offboard resources from IT services
- Own and maintain Corporate Security & IT Operations applications
- Document and enforce new and current IT policies and procedures
- Measure, monitor, and maintain team’s ability to meet or exceed contact and resolution Service Level Agreements (SLA)
- Run the IT Service desk, define and manage ticket SLAs, and move towards greater automation (and where appropriate) self-service of tickets
- Proven ability to successfully recruit, manage, motivate and develop high performing teams
- Negotiates and influences the opinions and decision making of internal senior leaders on matters of significance to the division.
- Consistently demonstrates, models and coaches managers and senior managers on GitLab's remote working competencies.
- Proactively communicates with leadership about progress and outcomes and how strategy and contributions support higher-level priorities and initiatives.
- Focuses the team’s communication and productivity


#### Senior Director, IT Corporate Security & Operations Requirements

- Bachelor's degree in IT, Computer Science, or related field
- 10+ years of IT and/or Security leadership experience
- 5+ years' experience in IT operations management, focusing on ITIL processes, service desk, identity and access management, infrastructure, and support.
- Strong understanding of Identity Management (SSO, SAML, OAuth, etc.), API integration (REST), Scripting (Bash, Powershell)
- SaaS experience: Expert level understanding of tools like Google Workspace, Okta, Zoom, Slack etc.
- InfoSec experience - partnering with internal information security and compliance teams. SOX experience is a plus.
- Team building experience. Past experience managing a global support team.
- Experience working on a fleet of MacOS and ChromeOS endpoints  
- Proven ability to effectively lead and meet business objectives in a global, collaborative and high performance work environment.
- Change management knowledge and ability to operate effectively in fast-paced environment.
- Desirable: PMP, ITIL certification. CISSP or similar certification is a plus.
- Demonstrated experience in vendor management and capacity planning in a fast growth environment.
